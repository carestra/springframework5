package com.carestra.learn.springframework;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(5)
public class CommandLineRunnerImpl implements CommandLineRunner {
  @Autowired
  private SimpleRepository repository;

  public void run(String... args) throws Exception {
    // args = application arguments as a simple string array
    String message = repository.getMessage();

    if (message != null) {
      repository.setMessage(message + "\nUsing commandLineRunner!");
    } else {
      repository.setMessage("\nUsing commandLineRunner!");
    }
  }
}
