package com.carestra.learn.springframework;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(10)
public class ApplicationRunnerImpl implements ApplicationRunner {
  @Autowired
  private SimpleRepository repository;

  public void run(ApplicationArguments args) throws Exception {
    String message = repository.getMessage();

    if (message != null) {
      repository.setMessage(message + "\nUsing Application runner");
    } else {
      repository.setMessage("\nUsing Application runner");
    }
  }
}
