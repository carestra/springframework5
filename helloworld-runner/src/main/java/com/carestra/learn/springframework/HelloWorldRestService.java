package com.carestra.learn.springframework;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldRestService {
  @Autowired
  private SimpleRepository repository;

  @RequestMapping("/")
  public String home() {
    return "Hello World with (application/CommandLine) runners!\n" + repository.getMessage();
  }

}
