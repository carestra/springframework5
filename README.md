    SpringFramework 5

Resources and program snippets using SpringFramework 5

### Structuring you code
https://docs.spring.io/spring-boot/docs/2.0.0.M4/reference/html/using-boot-structuring-your-code.html



    Spring CLI
    
The Spring Boot CLI is a command line tool that can be used if you want to quickly prototype with Spring. It allows you to run Groovy scripts, which means that you have a familiar Java-like syntax, without so much boilerplate code.

You don’t need to use the CLI to work with Spring Boot but it’s definitely the quickest way to get a Spring application off the ground.    

An example of a groovy script that runs a simple rest endpoint:

    @RestController
    class ThisWillActuallyRun {
        @RequestMapping("/")
        String home() {"Hello World!"}
    }

    Resources

Cloud
- https://www.infoq.com/presentations/spring-boot-netflix

Devops
- https://www.infoq.com/presentations/spring-boot-intellij

Demistify
- https://www.youtube.com/watch?v=uof5h-j0IeE&feature=youtu.be

Productive with Spring boot:
- https://www.infoq.com/presentations/spring-boot-dev-tools-productivity?utm_source=presentations_about_Spring-Boot&utm_medium=link&utm_campaign=Spring-Boot
- https://www.infoq.com/presentations/testing-spring-4-2
- https://www.infoq.com/presentations/spring-boot-intellij


Test projects
- https://github.com/spring-projects/spring-boot/tree/v2.0.0.M4/spring-boot-samples

Maven
- http://books.sonatype.com/mvnex-book/reference/index.html?__hstc=239247836.3748cde347021af5e249e64a68ae6e1d.1508008600982.1508008600982.1508008600982.1&__hssc=239247836.2.1508008600982&__hsfp=2831143117&__utma=246996102.1864065336.1508008600.1508008600.1508008600.1&__utmb=246996102.4.10.1508008600&__utmc=246996102&__utmx=-&__utmz=246996102.1508008600.1.1.utmcsr=books.sonatype.com|utmccn=(referral)|utmcmd=referral|utmcct=/mvnref-book/reference/&__utmv=-&__utmk=120228235